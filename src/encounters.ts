import { Danger, GroupLevel, RecommendedDifficulty, EncounterParameters, Creature} from './types'
import { Creatures } from './store';
import { uniqueCreatures } from './functions';

const RECOMMENDATIONS = {
    Starting: {
        Easy: { min: 1, max: 3},
        Average: { min: 4, max: 15 },
        Challenging: { min: 16, max: 30 },
        Hard: { min: 31, max: 0},
        dailyDifficulty: 25,
        maxCreatureDifficulty: 25
    },
    Novice: {
        Easy: { min: 1, max: 10 },
        Average: { min: 11, max: 30 },
        Challenging: { min: 31, max: 50},
        Hard: { min: 51, max: 0 },
        dailyDifficulty: 100,
        maxCreatureDifficulty: 100
    },
    Expert: {
        Easy: { min: 1, max: 30 },
        Average: { min: 31, max: 50 },
        Challenging: { min: 51, max: 125 },
        Hard: { min: 126, max: 0},
        dailyDifficulty: 200,
        maxCreatureDifficulty: 250
    },
    Master: {
        Easy: { min: 1, max: 50 },
        Average: { min: 51, max: 125 },
        Challenging: { min: 126, max: 200 },
        Hard: { min: 201, max:0 },
        dailyDifficulty: 500,
    }
}

export class Encounter {
    maxCreatureDifficulty = 0;
    danger = Danger.Easy
    groupLevel = GroupLevel.Starting;
    playerCount = 3;
    creatures: Creature[] = [];
    recommendedDifficulty: RecommendedDifficulty;
    difficulty = 0;

    constructor(parameters?: EncounterParameters) {
        if (parameters) {
            this.danger = parameters.danger;
            this.groupLevel = parameters.groupLevel;
            this.playerCount = parameters.playerCount;
        }
        this.determineRecommendedDifficulty();
        this.buildEncounter();
    }

    determineRecommendedDifficulty(): void {
        const levelRecommendations = RECOMMENDATIONS[this.groupLevel]
        let  playerFactor = 1

        if (this.playerCount < 3) playerFactor = 0.5;
        if (this.playerCount > 5) playerFactor = 2;
        this.recommendedDifficulty = {
            min: Math.max(Math.floor(levelRecommendations[this.danger].min * playerFactor), 1),
            max: Math.floor(levelRecommendations[this.danger].max * playerFactor),
        };

        this.maxCreatureDifficulty = levelRecommendations['maxCreatureDifficulty'];
    }

    buildEncounter(): void {
        const unsubscribe = Creatures.subscribe((creatures) => {
            let possibleCreatures = creatures;
            if (this.maxCreatureDifficulty) {
                possibleCreatures = possibleCreatures.filter(c => c.difficulty <= this.maxCreatureDifficulty)
            }
            if (possibleCreatures.length == 0) return
            const budgetRemaining = true;

            let chance = 0.75
            while (budgetRemaining) {
                const probabilty = Math.random();
                const reachedMinDifficulty = this.difficulty >= this.recommendedDifficulty.min;
                let fittingCreatures = possibleCreatures
                if (this.recommendedDifficulty.max > 0) {
                    const remainingBudget = this.recommendedDifficulty.max - this.difficulty;
                    const difficultyRange = this.recommendedDifficulty.max - this.recommendedDifficulty.min;
                    // chance ranges from 1 to 0, decreasing the chance of new creatures
                    chance = remainingBudget / difficultyRange;
                    fittingCreatures = fittingCreatures.filter(c => c.difficulty <= remainingBudget);
                }

                if (fittingCreatures.length == 0 || reachedMinDifficulty && probabilty > chance) {
                    break;
                }

                const selectedCreature = fittingCreatures[Math.floor(Math.random() * fittingCreatures.length)];
                this.creatures.push(selectedCreature)
                this.difficulty += selectedCreature.difficulty;

                /* Outnumbering the Group
                If the number of hostile creatures is double the number
                of characters in the group(rounding down), the danger
                increases by one step.
                */
                const playersOutnumbered = Math.floor(this.creatures.length) / 2 > this.playerCount
                if (this.danger != Danger.Hard && playersOutnumbered ) {
                    break
                }
            }
        })

        unsubscribe();
    }

    getUniqueCreatures(): Creature[] {
        return uniqueCreatures(this.creatures);
    }

    getCreatureCounts(): Record<string, unknown> {
        const creatureCounts = {};
        this.creatures.forEach((c) => {
            if (creatureCounts[c.name]) {
                creatureCounts[c.name]++;
            } else {
                creatureCounts[c.name] = 1;
            }
        });
        return creatureCounts;
    }
}