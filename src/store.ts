import { writable } from "svelte/store";
import BESTIARY from "./bestiary";
import { DEFAULT_ENCOUNTER_PARAMS } from "./types";
import { uniqueCreatures } from './functions';

const books = {};
new Set(BESTIARY.map(({ book }) => book)).forEach(v => books[v] = true);

export const Creatures = writable(uniqueCreatures(BESTIARY));
export const SortedCreatures = writable(uniqueCreatures(BESTIARY))
export const Filters = writable({
    "descriptors": {},
    "difficulty": {},
    "books": books,
    "frightening": false,
    "horrifying": false,
    "magic": false,
});
export const Sorting = writable({
    "name": "",
    "difficulty": "",
    "book": "",
})
export const isBestiaryVisible = writable(true);
export const currentEncounters = writable();
export const currentEncounterParams = writable({ ...DEFAULT_ENCOUNTER_PARAMS});