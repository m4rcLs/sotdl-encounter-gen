
export enum Book {
    Core = 'Shadow of the Demon Lord Core',
    DemonLordsCompanion = "Demon Lord's Companion",
    OccultPhilosophy = 'Occult Philosophy',
    TerribleBeauty = 'Terrible Beauty',
    ExquisiteAgony = 'Exquisite Agony',
    HungerInTheVoid = 'The Hunger in the Void',
    FreeportCompanion = 'Freeport Companion',
    BlackSunDawn = 'Black Sun Dawn',
    BeyondTheWorldsEdge = "Beyond the World's Edge",
    TombsOfDesolation = 'Tombs of the Desolation',
    MenOfGog = 'Men of Gog',
    CurseOfTheSpiderWood = 'Curse of the Spider Wood',
}

export type Creature = {
    name: string;
    difficulty: number;
    size: string;
    descriptors: string[];
    book: Book;
    page: number;
    frightening: boolean;
    horrifying: boolean;
    hasMagic: boolean;
};

export enum Danger {
    Easy = 'Easy',
    Average = 'Average',
    Challenging = 'Challenging',
    Hard = 'Hard',
}

export enum GroupLevel {
    Starting = 'Starting',
    Novice = 'Novice',
    Expert = 'Expert',
    Master = 'Master',
}

export type RecommendedDifficulty = {
    min: number;
    max: number;
}

export type EncounterParameters = {
    danger: Danger,
    groupLevel: GroupLevel,
    playerCount: number,
}

export const DEFAULT_ENCOUNTER_PARAMS: EncounterParameters =
    { playerCount: 3, groupLevel: GroupLevel.Starting, danger: Danger.Easy }
