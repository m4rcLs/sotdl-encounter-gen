import { Creature } from "./types";

export function uniqueCreatures(creatures: Creature[]): Creature[] {
    const uniqueCreatures = [];
    const seenCreatures = {};
    creatures.forEach((c) => {
        if (!seenCreatures[c.name]) {
            seenCreatures[c.name] = 1;
            uniqueCreatures.push(c);
        }
    });

    return uniqueCreatures;
}