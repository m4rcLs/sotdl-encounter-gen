import {writable, Writable} from 'svelte/store';
type UIState = {
    showBooks: boolean,
    showDescriptors: boolean,
    showBestiary: boolean,
};
export const UI: Writable<UIState> = writable({ showBooks: true, showDescriptors: true, showBestiary: true, })

export function showBestiary(): void {
    UI.update((ui) => {
        ui.showBestiary = true;
        return ui;
    });
}
export function showEncounters(): void {
    UI.update((ui) => {
        ui.showBestiary = false;
        return ui;
    });
}

export function toggleShowDescriptors(): void {
    UI.update((ui) => {
        ui.showDescriptors = !ui.showDescriptors;
        return ui;
    });
}

export function toggleShowBooks(): void {
    UI.update((ui) => {
        ui.showBooks = !ui.showBooks;
        return ui;
    });
}